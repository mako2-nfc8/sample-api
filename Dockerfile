FROM golang:latest as builder

ENV CGO_ENABLED=0
ENV GOOS=linux
ENV GOARCH=amd64
WORKDIR /go/src/sample-api
COPY . .
RUN go get github.com/ant0ine/go-json-rest/rest
RUN go build

# runtime image
FROM alpine
RUN apk add --no-cache ca-certificates
COPY --from=builder /go/src/sample-api/main /main
EXPOSE 8080
ENTRYPOINT ["/main"]