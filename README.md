# sample-api

## Create container image

```shell
docker build -t sample-app:latest .
```

## Run container

```shell
docker run --name sample-api --publish 8080:8080 sample-api:latest
```